package
{

	import net.flashpunk.Engine;
	import net.flashpunk.FP;
	import entities.Player;
	import entities.Cursor;
	import PrototypeWorld;

	public class Main extends Engine
	{
		public var player:Player;
		public var cur:Cursor;
		public function Main()
		{
	
			cur = new Cursor();
			player = new Player(cur);
			super(800, 600, 60, false);
			FP.world = new PrototypeWorld(player, cur);
			

		}
		
		
	}
}

package  
{
	/**
	 * ...
	 * @author Zach
	 */
	import entities.enemies.BeamBoss;
	import entities.enemies.swarmSpawner;
	import entities.Player;
	import net.flashpunk.FP;
	import net.flashpunk.Entity;
	import entities.enemies.swarmer;
	import entities.enemies.Arch;
	import entities.countdownText


	public class LevelController extends Entity 
	{
		var killCount:Number;
		public var time:Number;
		public var nextSpawnTime:Number;
		public var spawnTimer: Number;
		var player:Player;
		public var bossPhase:Boolean;
		public var bossKilled:Boolean;
		public var bossTimer:int;
		public var boss:String;
		
		public function LevelController(P:Player) 
		{
			player = P;
			var e = new swarmSpawner(P, [0, 0], this)
			FP.world.add(e)
			time = 0;
			spawnTimer = 45;
			nextSpawnTime = 180;
			bossPhase = false;
			bossKilled = false;
			bossTimer = 900;
			boss = ""
			FP.world.add(new countdownText())
		}
		
		public override function update():void
		{
			time ++;
			
			if (time > bossTimer && bossPhase == false)
			{
				boss = FP.choose("Missile", "Beam")
				boss = "Missile"
				if (boss == "Missile")
				{
					bossPhase = true;
					FP.world.add(new swarmSpawner(player, [20,20], this))
				}
				if (boss == "Beam")
				{
					bossPhase = true;
					FP.world.add(new BeamBoss(player, [20, 20], this))
					
				}
				
				
			}
			
			if (bossPhase == true && boss == "Beam" && nextSpawnTime < time)
			{
				var spawnpoint = FP.choose([20, 580], [780, 580])
				FP.world.add(new swarmer(player, spawnpoint))
				nextSpawnTime = time + 130;
			}
			
			if (time == nextSpawnTime && bossPhase == false )
			{
				
				if (player.Level <= 2 && bossPhase == false )
				{
					var spawnpoint = FP.choose([20, 20], [780, 20], [20, 580], [780, 580])
					var spawnType = FP.choose("Arch", "Swarmer", "Swarmer", "Swarmer", "Swarmer");
					
					if (spawnType == "Arch")
					{
						FP.world.add(new Arch(player, spawnpoint, this))
					}
					
					if (spawnType == "Swarmer")
					{
						FP.world.add(new swarmer(player, spawnpoint))
					}
					
					nextSpawnTime = spawnTimer + time;
				}
				
				
				if (player.Level >= 3)
				{
					spawnTimer = 15;
					
					var spawnpoint = FP.choose([20, 20], [780, 20], [20, 580], [780, 580])
					var spawnType = FP.choose("Arch", "Arch", "Swarmer", "Swarmer", "Swarmer");
					
					if (spawnType == "Arch")
					{
						FP.world.add(new Arch(player, spawnpoint, this))
					}
					
					if (spawnType == "Swarmer")
					{
						FP.world.add(new swarmer(player, spawnpoint))
					}
					
					nextSpawnTime = spawnTimer + time;
				}
				
				
			}
		}
		
	}

}
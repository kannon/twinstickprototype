package entities 
{
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Text;
	import net.flashpunk.FP;
	
	/**
	 * ...
	 * @author Zach
	 */
	public class SplashText extends Entity 
	{
		
		public function SplashText(n:Number) 
		{
			var str = "Game over! Your score is: " + n.toString();
			this.graphic = new Text(str, 300, 300,0,0)
		}
		
	}

}
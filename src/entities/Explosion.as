package entities 
{
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.FP;
	import net.flashpunk.tweens.misc.Alarm;
	
	
	/**
	 * ...
	 * @author Zach
	 */
	public class Explosion extends Entity 
	{
		[Embed(source = '../../img/boom.png')] private const BOOM:Class;
		[Embed(source = '../../img/boom1.png')] private const BOOM1:Class;
		[Embed(source = '../../img/boom2.png')] private const BOOM2:Class;
		public var TTL:Number;
		public var target:Player;
		
		public function Explosion(pos:Array, P:Player) 
		{
			this.x = pos[0]
			this.y = pos[1]
			graphic = new Image(BOOM);
			setHitbox(64, 64);
			type = "Explosion";
			addTween(new Alarm(0.5, this.death));
			target = P;
			this.TTL = 8;
		}
		public override function update():void
		{
			TTL --;
			if (TTL <= 0)
			{
				death();
			}
			
			var P:Player = collide("Player", x, y) as Player;
			
			if (P)
			{
				trace("Boom.")
				P.takeDamage(1);
			}
			
			if (target.Level >= 2)
			{
				setHitbox(64, 64);
				graphic = new Image(BOOM);
			}
			
			if (target.Level >= 4)
			{
				setHitbox(96,96);
				graphic = new Image(BOOM1);
			}
			
			if (target.Level == 5)
			{
				setHitbox(128,128);
				graphic = new Image(BOOM2);
			}
		}
		public function death():void
		{
			FP.world.remove(this);
		}
	}

}
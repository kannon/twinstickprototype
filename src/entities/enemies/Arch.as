package entities.enemies 
{
	import entities.EnemyShot;
	import entities.Player;
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Image;
	import Math;
	
	/**
	 * ...
	 * @author Zach
	 */
	public class Arch extends Entity 
	{
		[Embed(source = '../../../img/arch.png')] private const ARCH:Class;
		public var target:Player;
		public var moveDirectionX:Number;
		public var moveDirectionY:Number;
		public var changeDirectionTimer:Number;
		public var changeDirectionTime:Number;
		public var aimOffsetX:Number;
		public var aimOffsetY:Number;
		public var Controller:LevelController;
		public var movSpeed = 2;
		public var shotTimer:Number;
		public var shotTime:Number;
		
		public function Arch(player:Player, pos:Array, con:LevelController) 
		{
			setHitbox(16, 16);
			type = "Enemy"
			graphic = new Image(ARCH);
			this.x = pos[0];
			this.y = pos[1];
			target = player;
			Controller = con;
			changeDirectionTimer = 60;
			changeDirectionTime = 60;
			shotTime = 60;
			shotTimer = 5;
			moveDirectionX = Math.floor(Math.random() * 761) + 20;
			moveDirectionY = Math.floor(Math.random() * 561) + 20;
			aimOffsetX = Math.floor(Math.random() * 21) - 40;
			aimOffsetY = Math.floor(Math.random() * 21) - 40;
		}
		
		public override function update():void
		{
			if (shotTimer < Controller.time)
			{
				//This is where one would fire the laser.
				shotTimer = shotTime + Controller.time;
				var s = new EnemyShot([this.x + 12, this.y + 12],[target.x + aimOffsetX, target.y + aimOffsetY])
				FP.world.add(s);
			}
			
			if (changeDirectionTimer < Controller.time)
			{
				moveDirectionX = Math.floor(Math.random() * 761) + 20;
				moveDirectionY = Math.floor(Math.random() * 561) + 20;
				changeDirectionTimer = changeDirectionTime + Controller.time;
				
			}
		
			FP.stepTowards(this, moveDirectionX,moveDirectionY, movSpeed);
			
			if (target.Level >= 2)
			{
				shotTime = 70
			}
			
			if (target.Level >= 4)
			{
				shotTime = 45
			}
			
			if (target.Level == 5)
			{
				shotTime = 30
			}
			if (collide("playerFire", x, y))
			{
				target.modXP(10);
				boom();
			}
			
			if (collide("Player", x, y))
			{
				target.takeDamage(1);
				boom();
			}
			
			if (collide("Explosion", x, y))
			{
				target.modXP(15);
				boom();
			}
		}
		
		public function boom():void
		{
			FP.world.remove(this);
		}
		
	}

}
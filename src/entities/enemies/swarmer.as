package entities.enemies 
{
	import entities.Explosion;
	import entities.Player;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	
	/**
	 * ...
	 * @author Zach
	 */
	public class swarmer extends Entity 
	{
		[Embed(source = '../../../img/swarmer.png')] private const SWARMER:Class;
		[Embed(source = '../../../img/swarmer1.png')] private const SWARMER1:Class;
		[Embed(source = '../../../img/swarmer2.png')] private const SWARMER2:Class;
		public var direction:Array;
		public var target:Player;
		public var movSpeed = 2;
		
		public function swarmer(player:Player, pos:Array) 
		{
			setHitbox(16, 16);
			graphic = new Image(SWARMER);
			type = "Enemy"
			this.x = pos[0];
			this.y = pos[1];
			target = player;
			
		}
		
		public override function update():void
		{
			FP.stepTowards(this, target.x, target.y, movSpeed);
			
			if (collide("playerFire", x, y))
			{
				target.modXP(5);
				boom();
			}
			
			if (collide("Player", x, y))
			{
				boom();
			}
			
			if (collide("Explosion", x, y))
			{
				target.modXP(10);
				boom();
			}
			
			if (target.Level >= 2)
			{
				setHitbox(16, 16);
				graphic = new Image(SWARMER);
			}
			
			if (target.Level >= 4)
			{
				setHitbox(24, 24);
				graphic = new Image(SWARMER1);
			}
			
			if (target.Level == 5)
			{
				setHitbox(32, 32);
				graphic = new Image(SWARMER2);
			}
		}
		
		public function boom():void
		{
			FP.world.add(new Explosion([this.x - 44, this.y - 44], target));
			FP.world.remove(this);
		}
		
	}

}
package entities.enemies 
{
	import net.flashpunk.graphics.Image;
	import entities.missile;
	import net.flashpunk.Entity;
	import entities.Player;
	import net.flashpunk.FP;
	import entities.Explosion;
	
	/**
	 * ...
	 * @author Zach
	 */
	public class swarmSpawner extends Entity 
	{
		[Embed(source = '../../../img/boss1.png')] private const BOSS1:Class;
		public var target:Player;
		public var moveDirectionX:Number;
		public var Health:int;
		public var moveDirectionY:Number;
		public var changeDirectionTimer:Number;
		public var changeDirectionTime:Number;
		public var aimOffsetX:Number;
		public var aimOffsetY:Number;
		public var Controller:LevelController;
		public var movSpeed = 2;
		public var shotTimer:Number;
		public var shotTime:Number;
		
		public function swarmSpawner(player:Player, pos:Array, con:LevelController) 
		{
			setHitbox(128, 128);
			type = "EnemyBoss"
			graphic = new Image(BOSS1);
			this.x = pos[0];
			this.y = pos[1];
			target = player;
			Controller = con;
			changeDirectionTimer = 60;
			changeDirectionTime = 60;
			Health = 30;
			shotTime = 60;
			shotTimer = 5;
			moveDirectionX = Math.floor(Math.random() * 761) + 20;
			moveDirectionY = Math.floor(Math.random() * 561) + 20;
			aimOffsetX = Math.floor(Math.random() * 21) - 40;
			aimOffsetY = Math.floor(Math.random() * 21) - 40;
		}
		
		public override function update():void
		{
			if (shotTimer < Controller.time)
			{
				//This is where one would fire the laser.
				shotTimer = shotTime + Controller.time;
				var s = new missile(target,[this.x+64, this.y+65])
				FP.world.add(s);
			}
			
			if (changeDirectionTimer < Controller.time)
			{
				moveDirectionX = Math.floor(Math.random() * 761) + 20;
				moveDirectionY = Math.floor(Math.random() * 561) + 20;
				changeDirectionTimer = changeDirectionTime + Controller.time;
				
			}
		
			FP.stepTowards(this, moveDirectionX, moveDirectionY, movSpeed);

		}
		
		public function takeDamage(n:int)
		{
			Health--;
			if (Health <= 0)
			{
				boom();
			}
		}
		
		public function boom():void
		{
			//FP.world.add(new Explosion([this.x - 44, this.y - 44], target));
			Controller.bossPhase = false;
			Controller.bossKilled = true;
			target.modXP(200)
			Controller.nextSpawnTime = Controller.time + 15
			FP.world.remove(this);
		}
	}

}
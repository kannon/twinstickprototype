package entities.enemies 
{
	import entities.deathbeam;
	import net.flashpunk.graphics.Image;
	import entities.missile;
	import net.flashpunk.Entity;
	import entities.Player;
	import net.flashpunk.FP;
	import entities.Explosion;
	import entities.ChargingIndicator;
	
	/**
	 * ...
	 * @author Zach
	 */
	public class BeamBoss extends Entity 
	{
		[Embed(source = '../../../img/Boss2.png')] private const BOSS2:Class;
		public var target:Player;
		public var moveDirectionX:Number;
		public var Health:int;
		public var moveDirectionY:Number;
		public var changeDirectionTimer:Number;
		public var changeDirectionTime:Number;
		public var aimOffsetX:Number;
		public var aimOffsetY:Number;
		public var Controller:LevelController;
		public var movSpeed = 2;
		public var shotTimer:Number;
		public var shotTime:Number;
		public var isFiring:Boolean;
		public var firingTime:Number;
		public var firingTimer:int;
		
		public function BeamBoss(player:Player, pos:Array, con:LevelController) 
		{
			setHitbox(128, 128);
			type = "EnemyBoss"
			graphic = new Image(BOSS2);
			this.x = pos[0];
			this.y = pos[1];
			target = player;
			Controller = con;
			Health = 90;
			shotTime = 180;
			shotTimer = 180;
			firingTime = 60;
			firingTimer = 0;
			moveDirectionX = Math.floor(Math.random() * 761) + 20;
		}
		
		public override function update():void
		{
			if (shotTimer < Controller.time)
			{
				//This is where one would fire the laser.
				shotTimer = shotTime + Controller.time;
				isFiring = true;
				firingTimer = Controller.time + firingTime;
				//var s = new missile(target,[this.x+64, this.y+65])
				//FP.world.add(s);
			}
			if (Controller.time > firingTimer)
			{
				FP.stepTowards(this, target.x - 32, this.y, movSpeed);
			}
			
			if (shotTimer + 15 <= Controller.time)
			{
				FP.world.add(new ChargingIndicator(Controller, this))
			}
			if (isFiring == true)
			{
				FP.world.add(new deathbeam([this.x + 24, this.y + 80]))
				isFiring = false;
			}

		}
		
		public function takeDamage(n:int)
		{
			Health--;
			if (Health <= 0)
			{
				boom();
			}
		}
		
		public function boom():void
		{
			//FP.world.add(new Explosion([this.x - 44, this.y - 44], target));
			Controller.bossPhase = false;
			target.modXP(200)
			Controller.nextSpawnTime = Controller.time + 15
			Controller.bossTimer = Controller.time + 900
			FP.world.remove(this);
		}
	}

}
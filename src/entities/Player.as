package entities 
{
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.utils.Input;
	import net.flashpunk.utils.Key;
	import net.flashpunk.FP;
	import entities.SplashText;
	import entities.playerShot;
	
	/**
	 * ...
	 * @author Zach
	 */
	public class Player extends Entity 
	{
		[Embed(source = '../../img/playerTri.png')] private const PLAYERTRI:Class;
		[Embed(source = '../../img/playerSquare.png')] private const PLAYERSQUARE:Class;
		[Embed(source = '../../img/playerPent.png')] private const PLAYERPENT:Class;
		[Embed(source = '../../img/playerHex.png')] private const PLAYERHEX:Class;
		[Embed(source = '../../img/playerSphere.png')] private const PLAYERSPHERE:Class;
		public var aimCursor:Cursor;
		public var Controller:LevelController;
		public var Health:int;
		public var XP:Number;
		public var Score:Number;
		public var canFire:Boolean;
		public var fireRate:Number;
		public var resetTime:Number;
		public var Time:Number;
		public var immuneTime:Number;
		public var immuneTimer:Number;
		public var Level:int;
		public function Player(C:Cursor) 
		{
			//Controls!
			Input.define("Forward", Key.W, Key.UP);
			Input.define("Back", Key.S, Key.DOWN);
			Input.define("Left", Key.A, Key.LEFT);
			Input.define("Right", Key.D, Key.RIGHT);
			Input.define("Fire", Key.SPACE);
			
			
			//Setup image and start.
			setHitbox(32, 32);
			type = "Player"
			canFire = true;
			x = 300;
			y = 300;
			Health = 1;
			Level = 1;
			XP = 0;
			Score = 0;
			Time = 0;
			immuneTime = 70;
			fireRate = 10;
			aimCursor = C;
			resetTime = 0;
			setImage();
			
		}
		
		public override function update():void
		{
			Time ++;
			if (resetTime <= Time)
			{
				canFire = true;
			}
			if (immuneTimer <= Time)
			{
				this.type = "Player";
			}
			
			Image(graphic).angle = FP.angle(this.x, this.y, aimCursor.x, aimCursor.y) - 90;
		
			if (Input.check("Left")) { x -= 5; }
			if (Input.check("Right")) { x += 5; }
			if (Input.check("Forward")) { y -= 5; }
			if (Input.check("Back")) { y += 5; }
			if (Input.check("Fire") || Input.mouseDown && canFire == true)
			{
				fireMain();
				canFire = false;
				resetTime = fireRate + Time;
			}
			
			
		}
		public function takeDamage(d:Number)
		{
			//Health = Health - d;
			Health--;
			Level--;
			trace(Health, Level);
			setImage();
			type = "PlayerImmune";
			immuneTimer = immuneTime + Time;
			
			if (Level <= 0)
			{
				death();
			}
		}
		
		public function setImage():void
		{
			//Sets the image according to health levels.
			
			if (Level == 1)
			{
				graphic = new Image(PLAYERTRI);
			}
			
			if (Level == 2)
			{
				graphic = new Image(PLAYERSQUARE);
			}
			
			if (Level == 3)
			{
				graphic = new Image(PLAYERPENT);
			}
			
			if (Level == 4)
			{
				graphic = new Image(PLAYERHEX);
			}
			
			if (Level == 5)
			{
				graphic = new Image(PLAYERSPHERE);
			}
			
			Image(graphic).originX = 16;
			Image(graphic).originY = 16;
		}
		
		public function modXP(x:Number)
		{
			this.XP += x;
			this.Score += x;
			trace(XP);
			
			if (XP >= 25)
			{
				if (Level < 2)
				{
					
					trace("Ding!");
					this.Health = 2;
					this.Level = 2;
					trace(Health);
					setImage();
					XP = 0;
				}
			}
			
			if (XP >= 50)
			{
				if (Level > 3)
				{
				trace("Ding!");
				this.Health = 3;
				this.Level = 3;
				trace(Health);
				setImage();
				XP = 0;
				}
			}
			
			if (XP >= 75)
			{
				trace("Ding!");
				if (Level > 4)
				{
					this.Health = 4;
					this.Level = 4;
					trace(Health);
					setImage();
					XP = 0;
				}
			}
			
			if (XP >= 100)
			{
				trace("Ding!");
				if (this.Level > 5)
				{
					this.Health = 5;
					this.Level = 5;
					trace(Health);
					setImage();
					XP = 0;
				}
			}
		}
		public function fireMain():void
		{
			var s = new playerShot([this.x + 12, this.y + 12],[aimCursor.x - 4, aimCursor.y - 4])
			FP.world.add(s);
		}
		
		public function death():void
		{
			trace("Game over, man!")
			FP.world.add(new SplashText(this.Score))
			FP.world.remove(this);
			this.modXP( -50);
		}
	}

}
package entities 
{
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.utils.Input;
	
	/**
	 * ...
	 * @author Zach
	 */
	public class Cursor extends Entity 
	{
		[Embed(source = '../../img/cursor.png')] private const CURSOR:Class;
		
		public function Cursor() 
		{
			graphic = new Image(CURSOR);
			x = 100;
			y = 500;
		    Image(graphic).originX = 12;
			Image(graphic).originY = 12;
		}
		
		public override function update():void
		{
			//12 is the offset for the width of the cursor so it points at the middle. The sprite is 
			//24x24, so, 12. Quick, not pretty.
			x = Input.mouseX - 12;
			y = Input.mouseY - 12;
		}
		
		
	}

}
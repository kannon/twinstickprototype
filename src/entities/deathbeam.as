package entities 
{
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.FP;
	
	
	/**
	 * ...
	 * @author Zach
	 */
	public class deathbeam extends Entity 
	{
		[Embed(source = '../../img/deathbeam.png')] private const DEATHBEAM:Class;
		public var direction:Array;
		public var target:Player;
		public var movSpeed = 3;
		public var TTL:int = 180;
		public var time;
		public function deathbeam(pos:Array) 
		{
			this.x = pos[0]
			this.y = pos[1]
			graphic = new Image(DEATHBEAM);
			setHitbox(80, 700);
			type = "Explosion";
			this.TTL = 60;
		}
		
		public override function update():void
		{
			TTL --;
			if (TTL <= 0)
			{
				death();
			}
			
			var P:Player = collide("Player", x, y) as Player;
			
			if (P)
			{
				trace("Boom.")
				P.takeDamage(1);
			}
		}
		
		public function death()
		{
			FP.world.remove(this)
		}
	}

}
package entities 
{
	import entities.enemies.swarmSpawner;
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.FP;
	import Math
	
	/**
	 * ...
	 * @author Zach
	 */
	public class playerShot extends Entity 
	{
		[Embed(source = '../../img/playershot.png')] private const PLAYERSHOTSPRITE:Class;
		public var direction:Array;
		public var movSpeed = 10;
		public var dir;
		
		public function playerShot(Origin:Array ,D:Array) 
		{
			graphic = new Image(PLAYERSHOTSPRITE);
			this.x = Origin[0];
			this.y = Origin[1];
			Image(graphic).originX = 4;
			Image(graphic).originY = 4;
			setHitbox(8, 8);
			type = "playerFire"
			direction = D;
			dir = Math.atan2(direction[1] - this.y, direction[0] - this.x)*180/Math.PI
		}
		
		public override function update():void
		{
			this.x += Math.cos(dir * Math.PI / 180) * movSpeed;
			this.y += Math.sin(dir * Math.PI / 180) * movSpeed;
			
			if (collide("Enemy", x, y))
			{
				FP.world.remove(this)
			}
			
			var B:swarmSpawner = collide("EnemyBoss", x, y) as swarmSpawner;
		
			if (B)
			{
				B.takeDamage(1);
				FP.world.remove(this);
			}
		}
		
	}

}
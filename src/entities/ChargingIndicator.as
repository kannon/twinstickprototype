package entities 
{
	import entities.enemies.BeamBoss;
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.FP;
	import LevelController;
	
	/**
	 * ...
	 * @author Zach
	 */
	public class ChargingIndicator extends Entity 
	{
		[Embed(source = '../../img/boom.png')] private const CHARGE:Class;
		public var TTL:int;
		public var Controller:LevelController;
		public var Boss:BeamBoss;
		
		
		public function ChargingIndicator(cont:LevelController, boss:BeamBoss) 
		{
			Controller = cont;
			TTL = 20 + Controller.time;
			graphic = new Image(CHARGE);
			x = boss.x;
			y = boss.y;
			Boss = boss;
		}
		
		override public function update():void 
		{
			x = Boss.x
			y = Boss.y
			
			if (TTL > Controller.time)
			{
				FP.world.remove(this)
			}
		}
	}

}
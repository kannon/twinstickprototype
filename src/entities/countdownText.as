package entities 
{
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Text;
	
	/**
	 * ...
	 * @author Zach
	 */
	public class countdownText extends Entity 
	{
		public var time:int;
		public function countdownText() 
		{
			time = 0;
			this.graphic = new Text("In 3...", 300, 300,0,0)
		}
		
		public override function update():void
		{
			time++;
			
			if (time > 50 && time < 110)
			{
				graphic = new Text("In 2...", 300, 200)
			}
			
			if (time > 110 && time < 175)
			{
				graphic = new Text("In 1...", 300, 200)
			}
			
			if (time > 176 && time < 190)
			{
				graphic = new Text("Go!", 300, 200)
			}
			
			if (time > 190)
			{
				FP.world.remove(this);
			}
		}
		
	}

}
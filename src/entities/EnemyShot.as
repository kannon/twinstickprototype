package entities 
{
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.FP;
	import Math
	
	/**
	 * ...
	 * @author Zach
	 */
	public class EnemyShot extends Entity 
	{
		[Embed(source = '../../img/enemyshot.png')] private const ENEMYSHOT:Class;
		public var direction:Array;
		public var movSpeed = 5;
		public var dir;
		
		public function EnemyShot(Origin:Array ,D:Array) 
		{
			graphic = new Image(ENEMYSHOT);
			this.x = Origin[0];
			this.y = Origin[1];
			Image(graphic).originX = 4;
			Image(graphic).originY = 4;
			setHitbox(8, 8);
			type = "enemyFire"
			direction = D;
			dir = Math.atan2(direction[1] - this.y, direction[0] - this.x)*180/Math.PI
		}
		
		public override function update():void
		{
			this.x += Math.cos(dir * Math.PI / 180) * movSpeed;
			this.y += Math.sin(dir * Math.PI / 180) * movSpeed;
			
			var P:Player = collide("Player", x, y) as Player;
			
			if (P)
			{
				trace("Boom.")
				P.takeDamage(1);
				FP.world.remove(this);
			}
		}
		
	}

}
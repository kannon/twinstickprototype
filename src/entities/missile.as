package entities 
{
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Image;
	
	/**
	 * ...
	 * @author Zach
	 */
	public class missile extends Entity
	{
		[Embed(source = '../../img/missiles.png')] private const BOSSMISSILE:Class;
		public var direction:Array;
		public var target:Player;
		public var movSpeed = 3;
		public var TTL:int = 180;
		public var time;
		
		public function missile(player:Player, pos:Array) 
		{
			setHitbox(24, 24);
			graphic = new Image(BOSSMISSILE);
			type = "Enemy"
			this.x = pos[0];
			this.y = pos[1];
			time = 0;
			target = player;
		}
		
		public override function update():void
		{
			time++;
			if (time >= TTL)
			{
				boom();
			}
			FP.stepTowards(this, target.x, target.y, movSpeed);
			
			
			if (collide("Player", x, y))
			{
				boom();
			}
			
			if (collide("Explosion", x, y))
			{
				target.modXP(10);
				boom();
			}
		}
		
		public function boom():void
		{
			FP.world.add(new Explosion([this.x - 12, this.y - 12], target));
			FP.world.remove(this);
		}
	}
		
	

}
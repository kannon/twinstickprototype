package  
{
	import entities.Cursor;
	import entities.enemies.swarmer;
	import net.flashpunk.World;
	import entities.Player;
	import net.flashpunk.FP;
	
	/**
	 * ...
	 * @author Zach
	 */
	public class PrototypeWorld extends World 
	{
		
		public function PrototypeWorld(P:Player, C:Cursor) 
		{
			add(P);
			add(C);
			add(new LevelController(P));
		}
		
	}

}